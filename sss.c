/*
  Project 6:
  This program simulates a submit server. It takes in two files, one
  with compilation code and another with execution code. This simple
  submit server compiles the code first. Reporting failure if any
  of the compilation commands fail for whatever reason. Then it 
  executes the code returning the number of correct executions
  done.
  The Commands structure that holds the data is two charachter
  double arrays to hold the lines of compilation and exeuction 
  from the files. The other two elements of the structure are two
  integers that describe the number of compilation and execution lines
  stored in the struct respectively.
  Author: Abdelrahman Hammad    Student ID: 112394561
  Section: 0104                 TA: Mathew
  Directory ID: ahammad 
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include "sss.h"
#include "sss-implementation.h"
#include "split.h"
#define MAX_BUFFER 257

/* Function that takes a two file names that contain the compile and
   test commands to be run by simple submit server*/
Commands read_commands(const char *compile_cmds, const char *test_cmds) {
  FILE* compile_file;
  char buffer[MAX_BUFFER];
  FILE * test_file;
  Commands results = {NULL,0,NULL,0};
  int counter = 0;
  /*Checks whether parameters are null or not*/
  if (compile_cmds == NULL || test_cmds == NULL) {
    exit(0);
  }
  /*Attempts to open compile execution files*/
  compile_file = fopen(compile_cmds, "r");
  if(compile_file == NULL) {
    exit(0);
  }
  /*Attempts to open test execution files*/
  test_file = fopen(test_cmds,"r");
  if(test_file == NULL) {
    exit(0);
  }
  /*Gets a line from compile_file*/
  while(fgets(buffer, MAX_BUFFER, compile_file) != NULL) {
    /* If this is not the first line*/ 
    if(counter > 0) {
      results.compile_head = (char**)
	realloc(results.compile_head,
		sizeof(char*) * (counter + 1));
      /* Check memory allocation*/
      if(results.compile_head == NULL) {
	printf("Allocation error");
	exit(1);
      }
    }else {
      results.compile_head = (char**) malloc(sizeof(char*));
      /* Check memory allocation*/
      if(results.compile_head == NULL) {
	printf("Allocation error");
	exit(1);
      }
    }
    results.compile_head[counter] = (char*)
      malloc ((strlen(buffer) + 1) * sizeof(char));
    /* Checking memory allocation*/
    if(results.compile_head[counter] == NULL) {
      printf("Allocation error");
      exit(1);
    }
    /* Copies the contents of the buffer into compile header double pointer */ 
    strcpy(results.compile_head[counter], buffer);
    counter++;
  }
  /* Setting the size to number of lines read*/
  results.compile_size = counter;
  counter = 0;
  /*Reading lines from test file */
  while(fgets(buffer, MAX_BUFFER, test_file) != NULL){
    if(counter > 0){
      results.test_head = (char**)
	realloc(results.test_head,
		sizeof(char*) * (counter + 1));
      /* Checking memory allocation */
      if(results.test_head == NULL){
	printf("Allocation error");
	exit(1);
      }
    }else{
      results.test_head = (char**) malloc(sizeof(char*));
       /* Checking memory allocation */
      if(results.test_head == NULL){
	printf("Allocation error");
	exit(1);
      }
    }
    results.test_head[counter] = (char*)
      malloc ((strlen(buffer) + 1)*sizeof(char));
     /* Checking memory allocation*/
    if(results.test_head[counter] == NULL){
      printf("Allocation error");
      exit(1);
    }
    strcpy(results.test_head[counter], buffer);
    counter++;
  }
  results.test_size = counter;
  /* Closing the file streams*/
  fclose(compile_file);
  fclose(test_file);
  return results;
}

int compile_program(Commands commands) {
  int status;
  pid_t id;
  char** args;
  int counter = 0;
  int eraser = 0;
  /* Checks whether there is any compile size */
  if(commands.compile_size == 0)
    return 0;
  /* Evaluating all the commands to be compiled*/
  while( counter < commands.compile_size ) {
    args = split(commands.compile_head[counter]);
    id = fork();
    /* Parent*/
    if( id > 0 ) {
      /* Wait for child process to complete*/
      wait(&status);
      /*Freeing args double array*/
      while(args[eraser] != NULL) {
	free(args[eraser]);
	args[eraser] = NULL;
	eraser++;
      }
      free(args);
      args = NULL;
      eraser = 0;
      /* If child failed return 0*/
      if(WIFEXITED(status) == 0 || WEXITSTATUS(status) != 0 )
	return 0;
      /*Child process*/
    } else if (id == 0) {
      execvp(args[0],args);
      return 0;
    }else {
      /*If forking fails*/
      printf("Forking Error");
      exit(EXIT_FAILURE);
    }
    counter++;
  }
  /* Compilation was correct*/
  return 1;
}
/* Function that executes teh test commands in the commands structure*/
int test_program(Commands commands) {
  int status;
  pid_t id;
  char** args;
  int counter = 0;
  int eraser = 0;
  int successful = 0;
  int seeker = 0;
  int fd;
  int fd_2;
  /* If there are no test commands return 0*/
  if(commands.test_size == 0)
    return 0;
  /* Evaluating all the tests to be executed*/
  while( counter < commands.test_size ) {
    args = split(commands.test_head[counter]);
    id = fork();
    /* Parent process*/
    if( id > 0 ) {
      /*Wait for the child process*/
       wait(&status);
       /* Freeing dynamically allocated memory in args[]*/
      while(args[eraser] != NULL) {
	free(args[eraser]);
	args[eraser] = NULL;
	eraser++;
      }
      free(args);
      args = NULL;
      eraser = 0;
      /* If child process was successful add one*/
      if(WIFEXITED(status) != 0 && WEXITSTATUS(status) == 0 )
	successful++;
      /* Child process*/
    } else if (id == 0) {
      /* Checking if input/output redirection is reuqired*/
      while(args[seeker] != NULL) {
	seeker++;
      }
      /* If there are atleast 2 arguments then check for output*/
      if(seeker > 1 && strcmp(args[seeker - 2],">") == 0 ) {
	/* Open file and redirect output to that file*/
	fd = open(args[seeker-1],O_WRONLY);
	dup2(fd, 1);
	/* If there are atleast 2 arguments then check for input*/
      }else if (seeker > 1 && strcmp(args[seeker - 2],"<") == 0 ) {
	/* Open file and redirect input to that file*/
	fd = open(args[seeker-1],O_RDONLY );
	dup2(fd,0);
      }
       /* If there are atleast 4 arguments then check for second redirection*/
      if(seeker > 3 && strcmp(args[seeker - 4],">") == 0 ) {
	/* Open file and redirect output to that file*/
	fd_2 = open(args[seeker-3],O_WRONLY);
	dup2(fd_2, 1);
	/* If there are atleast 4 arguments then check for second redirection*/
      }else if (seeker > 3 && strcmp(args[seeker - 4],"<") == 0 ) {
	/* Open file and redirect input to that file*/
	fd_2 = open(args[seeker-3],O_RDONLY );
	dup2(fd_2,0);
      }
      /* Child executes arguments*/
      execvp(args[0],args);
      return 0;
    }else{
      /* If forking error occurs*/
      printf("Forking Error");
      exit(EXIT_FAILURE);
    }
    counter++;
  }
  /* Return number of successful operation*/
  return successful;
}

/* Function that clears all the dynamically allocated memory in the 
   coomands structure*/
void clear_commands(Commands *const commands) {
  int counter = 0;
  /* Go through all compile commands*/
  if(commands != NULL) {
    while(counter < commands->compile_size) {
      /*Free each char pointer in the char double pointer*/
      free(commands->compile_head[counter]);
      counter++;
    }
    /*Free character pointer*/
    free (commands->compile_head);
    commands->compile_head = NULL;
    counter = 0;
    /* Go through all test commands*/
    while(counter < commands->test_size) {
      /*Free each char pointer in the char double pointer*/
      free(commands->test_head[counter]);
      counter++;
    }
    /*Free the char double pointer*/
    free (commands->test_head);
    commands->test_head = NULL;
  }
}

