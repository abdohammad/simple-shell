 This program simulates a submit server. It takes in two files, one
  with compilation code and another with execution code. This simple
  submit server compiles the code first. Reporting failure if any
  of the compilation commands fail for whatever reason. Then it 
  executes the code returning the number of correct executions
  done.
  The Commands structure that holds the data is two charachter
  double arrays to hold the lines of compilation and exeuction 
  from the files. The other two elements of the structure are two
  integers that describe the number of compilation and execution lines
  stored in the struct respectively.