#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include "sss.h"
#include "sss-implementation.h"
#include "split.h"
#define MAX_BUFFER 256

  
Commands read_commands(const char *compile_cmds, const char *test_cmds) {
  FILE* compile_file;
  char buffer[MAX_BUFFER];
  FILE * test_file;
  Commands results = {NULL,NULL};
  int counter = 0;
  /*Checks whether parameters are null or not*/
  if (compile_cmds == NULL || test_cmds == NULL) {
    printf("Arguments entered are null\n");
    exit(0);
  }
  /*Attempts to open compile execution files*/
  compile_file = fopen(compile_cmds, "r");
  if(compile_file == NULL) {
    printf("Compile file does not exist\n");
    exit(0);
  }
  /*Attempts to open test execution files*/
  test_file = fopen(test_cmds,"r");
  if(test_file == NULL) {
    printf("Test file does not exist\n");
    exit(0);
  }
  results.compile_head = (Compile_holder**) malloc(sizeof(Compile_holder*));
  results.test_head = (Test_holder**) malloc(sizeof(Test_holder*));
  if(results.compile_head == NULL ||  results.test_head == NULL){
    printf("Memory allocation error");
    exit(0);
  }

  while(fgets(buffer, MAX_BUFFER, compile_file) != NULL){
    if(counter>0)
      results.compile_head = (Compile_holder**)
	realloc(results.compile_head,
		sizeof(Compile_holder*) * (counter + 1));
    results.compile_head[counter] = (Compile_holder*)
      malloc(sizeof(Compile_holder));
    results.compile_head[counter]->data = (char*)
      malloc((strlen(buffer) + 1)*sizeof(char));
    strcpy(results.compile_head[counter]->data, buffer);
    counter++;
  }
  counter--;
  while(counter>0){
    results.compile_head[counter-1]->next = results.compile_head[counter];
    counter--;
  }
   while(fgets(buffer, MAX_BUFFER, test_file) != NULL){
     if(counter > 0)
       results.test_head = (Test_holder**)
	 realloc(results.test_head,
		 sizeof(Test_holder*) * (counter + 1));
    results.test_head[counter] = (Test_holder*)
      malloc(sizeof(Test_holder));
    results.test_head[counter]->data = (char*)
      malloc((strlen(buffer) + 1)*sizeof(char));
    results.test_head[counter]->next = NULL;
    counter++;
  }
   counter--;
   while(counter>0){
     results.test_head[counter-1]->next = results.test_head[counter];
     counter--;
   }

  
  
   return results;
}

int compile_program(Commands commands){
  Compile_holder *travel = *(commands.compile_head);
  pid_t id;
  char** args;
  if(commands.compile_head != NULL &&
     *(commands.compile_head) != NULL) {
    args = (char**) malloc(sizeof(char*));
    while( travel != NULL ){
      args = split(travel->data);
      id = fork();
      if( id > 0 ){
	wait(NULL);
	free(args);
	args = NULL;
      } else if (id == 0) {
	execvp(args[0],args);
	return 0;
      }
      travel = travel->next;
      args = (char**) realloc(args, sizeof(char*));
    }
    return 1;
  }
  return 0;
}


void clear_commands(Commands *const commands){
  Compile_holder* travel = NULL;
  Compile_holder* eraser = NULL;
  Test_holder* test_travel = NULL;
  Test_holder* test_eraser = NULL;
  if(commands!=NULL){
    travel = *(commands->compile_head);
    while(travel != NULL){
      free(travel -> data);
      travel ->data = NULL;
      eraser = travel;
      travel = travel->next;
      free(eraser);
      eraser = NULL;
    }
    free(commands->compile_head);
    commands->compile_head = NULL;
    test_travel = *(commands->test_head);
    while(test_travel != NULL){
      free(test_travel->data);
      test_travel->data = NULL;
      test_eraser = test_travel;
      test_travel = test_travel->next;
      free(test_eraser);
      test_eraser = NULL;
    }
    free(commands->test_head);
    commands->test_head = NULL;
  }


}


      



